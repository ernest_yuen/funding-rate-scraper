CREATE TABLE IF NOT EXISTS ftx_funding_rates (
    id BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    future VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL,
    rate DECIMAL(65,16) DEFAULT NULL,
    time TIMESTAMP,
    insert_timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    flag INT(11) DEFAULT 1,
    UNIQUE KEY ftx_funding_rates_future_time_uindex (future, rate),
    KEY ftx_funding_rates_future_time_index (future, rate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
