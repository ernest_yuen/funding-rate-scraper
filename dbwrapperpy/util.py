import datetime


def fetch_current_timestamp() -> str:
    """Fetch the current timestamp in forms of '%Y-%m-%d %H:%M:%S'.

    Returns:
        str: current timestamp in forms of '%Y-%m-%d %H:%M:%S'.

    """
    now = datetime.datetime.now()
    return now.strftime('%Y-%m-%d %H:%M:%S')


__all__ = (
    'fetch_current_timestamp',
)
