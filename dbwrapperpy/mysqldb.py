import subprocess
import math

import pandas as pd
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine.url import URL
from sqlalchemy import inspect
from sqlalchemy.ext.automap import automap_base

from dbwrapperpy.util import *


class MySQLWrapper(object):
    """A class as a wrapper for MySQL.

    To use:
    >>> db_details = {
            'username': 'user',
            'password': 'password#',
            'host': '127.0.0.1',
            'port': '3306',
            'database': 'db'
        }
    >>> mysql_db = MySQLWrapper(db_details=db_details)
    """
    def __init__(self, db_details: dict):
        """

        Args:
            db_details (dict): Database credentials.

        Returns:
            None

        """
        self._db_details = {'drivername': 'mysql+mysqlconnector'}
        for detail, val in db_details.items():
            self._db_details.update({detail: val})
        self._engine = sqlalchemy.create_engine(URL(**self._db_details))
        self._session = sessionmaker(bind=self._engine, autocommit=False, autoflush=True)
        self._base = automap_base()
        self._base.prepare(self._engine, reflect=True)

    @property
    def engine(self):
        return self._engine

    def execute_sql(self, sql_script: str) -> None:
        """Execute SQL script.

        Args:
            sql_script (str): SQL script.

        Returns:
            None

        """
        session = self._session()
        session.execute(sql_script)
        session.commit()
        session.close()

        return

    def fetch_count(self, table_name: str, column=None, min_timestamp=None, max_timestamp=None) -> int:
        """Fetch the count of valid entries based on a period of timestamp provided.

        Args:
            table_name (str): MySQL database table name.
            column (Union[None, str]): The corresponding column of filtering timestamp; default: 'insert_timestamp'
            min_timestamp (Union[None, str]): Minimum timestamp of the insert timestamp;
                                              default: '1990-01-01 00:00:00'
            max_timestamp (Union[None, str]): Maximum timestamp of the insert timestamp;
                                              default: Current timestamp in forms of '%Y-%m-%d %H:%M:%S'

        Returns:
            int: Number of counts of the filtered entries.

        Raises:
            ValueError: If table_name does not exist in the database table names.

        """
        inspector = inspect(self._engine)

        if table_name not in inspector.get_table_names():
            raise ValueError('Invalid table_name')

        if column is None:
            column = 'insert_timestamp'
        if min_timestamp is None:
            min_timestamp = '1990-01-01 00:00:00'
        if max_timestamp is None:
            max_timestamp = fetch_current_timestamp()

        query = """SELECT COUNT(*) FROM {0} WHERE {1} <= '{2}' AND {1} > '{3}'"""\
            .format(table_name, column, max_timestamp, min_timestamp)
        count_df = pd.read_sql(sql=query, con=self._engine)

        if count_df.loc[0].values[0] is None:
            return 0

        return count_df.loc[0].values[0]

    def fetch_latest_insert_timestamp(self, table_name: str, column_conditions=None) -> str:
        """Fetch the latest insert timestamp given the table name.

        Args:
            table_name (str): MySQL database table name.
            column_conditions (Union[None, dict]): Relation columns information in terms of a dictionary;
                                                   default: {'max_col': 'insert_timestamp', 'valid_col': 'flag'}

        Returns:
            str: Latest insert timestamp in the forms of '%Y-%m-%d %H:%M:%S'.

        Raises:
            ValueError: If table_name does not exist in the database table names.
            TypeError: If the latest insert timestamp is None.

        """
        inspector = inspect(self._engine)

        if table_name not in inspector.get_table_names():
            raise ValueError('Invalid table_name')

        if column_conditions is None:
            column_conditions = {'max_col': 'insert_timestamp', 'valid_col': 'flag'}
        query = """SELECT MAX({0}) FROM {1} WHERE {2}=1;"""\
            .format(column_conditions['max_col'], table_name, column_conditions['valid_col'])
        timestamp_df = pd.read_sql(sql=query, con=self._engine)
        if timestamp_df.loc[0].values[0] is None:
            raise TypeError('The latest insert timestamp is None, i.e. No corresponding latest insert_timestamp')

        return timestamp_df.loc[0].dt.strftime('%Y-%m-%d %H:%M:%S').values[0]

    def fetch_time_series_last_timestamp(self, table_name: str, column: str) -> str:
        """Fetch the latest timestamp given the table name.

        Args:
            table_name (str): MySQL database table name.
            column (str): Filtered Column for timestamp.

        Returns:
            str: Latest timestamp in the forms of '%Y-%m-%d %H:%M:%S'.

        Raises:
            ValueError: If table_name does not exist in the database table names.
            TypeError: If the latest timestamp is None.

        """
        inspector = inspect(self._engine)

        if table_name not in inspector.get_table_names():
            raise ValueError('Invalid table_name')

        query = """SELECT MAX({0}) FROM {1} WHERE flag=1;""".format(column, table_name)
        timestamp_df = pd.read_sql(sql=query, con=self._engine)
        if timestamp_df.loc[0].values[0] is None:
            raise TypeError('The {0} is None, i.e. No corresponding latest {0}'.format(column))

        return timestamp_df.loc[0].dt.strftime('%Y-%m-%d %H:%M:%S').values[0]

    def fetch_data(self, table_name: str, query=None) -> pd.DataFrame:
        """Fetch database data based given table name & query (if any).

        Args:
            table_name (str): MySQL database table name.
            query (Union[None, str]): SQL query; default: None

        Returns:
            pd.DataFrame: Corresponding dataframe.

        Raises:
            ValueError: If table_name does not exist in the database table names.

        """
        inspector = inspect(self._engine)

        if table_name not in inspector.get_table_names():
            raise ValueError('Invalid table_name')

        if query is None:
            df = pd.read_sql_table(table_name=table_name, con=self._engine)
        else:
            df = pd.read_sql(sql=query, con=self._engine)

        return df

    def _push_data(self, table_name: str, data: pd.DataFrame, columns=None) -> None:
        """Push data into database.

        Args:
            table_name (str): MySQL database table name.
            data (pd.DataFrame): Dataframe data.
            columns (Union[None, list]): Database columns.

        Returns:
            None

        Raises:
            ValueError: If table_name does not exist in the database table names.

        """
        inspector = inspect(self._engine)

        if table_name not in inspector.get_table_names():
            raise ValueError('Invalid table_name')

        data = data.where(pd.notnull(data), None)
        sql_query = """INSERT INTO {} SET""".format(table_name)
        if columns is None:
            updates = ",".join("{0} = :{0}".format(d) for d in list(data))
            sql_query = sql_query + "\n" + updates
            sql_query = sql_query + "\n" + "ON DUPLICATE KEY UPDATE"
            sql_query = sql_query + "\n" + updates
        else:
            updates = ",".join("{0} = :{0}".format(c) for c in columns)
            sql_query = sql_query + "\n" + updates
            sql_query = sql_query + "\n" + "ON DUPLICATE KEY UPDATE"
            sql_query = sql_query + "\n" + updates

        session = self._session()
        for i, row in data.iterrows():
            dict_input = dict(row)
            dict_input_clean = {}
            for k, v in dict_input.items():
                v = None if (pd.isnull(v)) else v
                dict_input_clean[k] = v

            session.execute(sql_query, dict_input)

        session.commit()
        session.close()

    def push_data(self, table_name: str, data: pd.DataFrame, batch_size=None, columns=None) -> None:
        """Push data into database.

        Args:
            table_name (str): MySQL database table name.
            data (pd.DataFrame): Dataframe data.
            batch_size (Union[None, int]): The batch size.
            columns (Union[None, list]): Database columns.

        Returns:
            None

        Raises:
            ValueError: If table_name does not exist in the database table names.

        """
        if batch_size is None:
            self._push_data(table_name=table_name, data=data, columns=columns)
        else:
            batch_no = math.ceil(len(data)/batch_size)

            for bn in range(batch_no):
                d = data[batch_size*bn: batch_size*(bn + 1)]
                self._push_data(table_name=table_name, data=d, columns=columns)

    def export_sql(self, export_directory: str):
        """Export all sql scripts in a database to a designated directory.

        Args:
            export_directory (str): Designated directory for all sql scripts.

        Returns:
            None

        """
        command_line = 'mysqldump --no-tablespaces -h {0} -P {1} -u {2} -p{3} {4}'.format(
            self._db_details['host'],
            self._db_details['port'],
            self._db_details['username'],
            self._db_details['password'],
            self._db_details['database']
        )
        with open(export_directory, 'w') as output:
            c = subprocess.Popen(command_line, stdout=output, shell=True)

        return


__all__ = (
    'MySQLWrapper',
)
