import time
import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed

import pandas as pd
import requests

from dbwrapperpy.mysqldb import MySQLWrapper


def fetch_tickers():
    base_url = 'https://ftx.com/api'
    funding_rates = '/funding_rates'
    headers = {'Content-Type': 'application/json'}
    req = requests.get('{}{}'.format(base_url, funding_rates), headers=headers)
    futures_tickers = [r['future'] for r in req.json()['result']]

    return futures_tickers


def fetch_hour_backtracer_unixtimestamp():
    now = datetime.datetime.utcnow()
    return int((now.replace(second=0, microsecond=0, minute=0, hour=now.hour)).timestamp())


def fetch_historical_funding_rate_data(ticker):
    base_url = 'https://ftx.com/api'
    funding_rates = '/funding_rates'
    # timestamp = fetch_hour_backtracer_unixtimestamp()
    timestamp = 1641445200
    count = 0
    time_diff = 3600
    data = []

    while count < 2500:
        r = requests.get('{}{}?start_time={}&end_time={}&future={}'
                         .format(base_url, funding_rates, timestamp, timestamp, ticker))
        contents = r.json()
        if contents.get('success'):
            if len(contents['result']) == 0:
                print(ticker, timestamp)
                print(ticker, 'DONE')
                break
            else:
                print(ticker, timestamp)
                data.append(contents['result'][0])
        else:
            print(ticker, timestamp)
            print(ticker, 'DONE')

        timestamp -= time_diff
        count += 1
        time.sleep(0.1)

    df = pd.DataFrame(data)
    df.to_csv('./generated/ftx_{}_funding_rate.csv'.format(ticker), index=False)
    # db_details = {
    #     'username': 'reader',
    #     'password': 'Reader149!',
    #     'host': '128.199.224.167',
    #     'port': '3306',
    #     'database': 'crypto_algo_trading'
    # }
    #
    # # Initialisation
    # mysql_db = MySQLWrapper(db_details=db_details)
    # mysql_db.push_data(table_name='ftx_funding_rates', data=df)


def main():
    tickers = fetch_tickers()
    # tickers = ['DOT-PERP', 'ADA-PERP', 'BNB-PERP', 'BTC-PERP', 'ETH-PERP', 'CREAM-PERP', 'FTM-PERP',
    #            'SOL-PERP', 'AVAX-PERP', 'MATIC-PERP', 'FTT-PERP', 'LUNA-PERP', 'SUSHI-PERP', 'MANA-PERP']
    tickers = ['AAVE-PERP', 'COMP-PERP', 'DOGE-PERP', 'ENS-PERP', 'LRC-PERP', 'RUNE-PERP',
               'SAND-PERP', 'SRM-PERP', 'SXP-PERP', 'UNI-PERP']
    for t in tickers:
        try:
            fetch_historical_funding_rate_data(t)
        except Exception as e:
            print(e.__str__())
            continue
    # for i in range(int(len(tickers)/5)):
    #     print(i)
    #     ticker = tickers[i:(i+1)*5]
    #     with ThreadPoolExecutor(max_workers=len(ticker)) as executor:
    #         futures = [executor.submit(fetch_historical_funding_rate_data, x) for x in ticker]
    #
    #     for future in as_completed(futures):
    #         print(future.result())


if __name__ == '__main__':
    main()
