import pandas as pd
import requests
from apscheduler.schedulers.blocking import BlockingScheduler

from dbwrapperpy.mysqldb import MySQLWrapper


def update():
    db_details = {
        'username': 'reader',
        'password': 'Reader149!',
        'host': '128.199.224.167',
        'port': '3306',
        'database': 'crypto_algo_trading'
    }

    # Initialisation
    mysql_db = MySQLWrapper(db_details=db_details)

    base_url = 'https://ftx.com/api'
    funding_rates = '/funding_rates'
    headers = {'Content-Type': 'application/json'}
    req = requests.get('{}{}'.format(base_url, funding_rates), headers=headers)

    df = pd.DataFrame(req.json()['result'])
    mysql_db.push_data(table_name='ftx_funding_rates', data=df)


if __name__ == '__main__':
    s = BlockingScheduler(timezone="Asia/Shanghai")
    s.add_job(
        update,
        trigger='cron',
        minute=1
    )

    try:
        s.start()
    except (KeyboardInterrupt, SystemExit):
        pass
